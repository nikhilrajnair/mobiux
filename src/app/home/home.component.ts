import { analyzeAndValidateNgModules } from '@angular/compiler';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { SalesService } from '../service/sales.service';

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}



@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;

  public mappedData: any = [];
  constructor(private SalesService: SalesService) { }
  displayedColumns: string[] = ['date', 'name', 'quantity', 'unit_price', 'total_price'];
  dataSource: any;
  public totalSale: number;
  monthSale: any;
  mostPopularinMonth
  ngOnInit(): void {
    this.getTableData();
  }

 

  getTableData() {
    this.SalesService.getJSON().subscribe(data => {
      this.mappedData = data;
      let mappedJsonData = [];
      let lines = this.mappedData.split("\n");
      let header = lines[0].split(",");
 
      for (let i = 1; i < lines.length - 1; i++) {
        let element = lines[i];
        let eacheElement = element.split(",");
        // console.log(eacheElement);
        let el: any = {};
        header.map((keys: any, index: number) => {
          el[keys] = eacheElement[index];
        })
        // console.log(el);
        mappedJsonData.push(el);
        // console.log(lines[i])
      }
      console.log(mappedJsonData);
      mappedJsonData = JSON.parse(JSON.stringify(mappedJsonData).replace(/\s(?=\w+":)/g, ""));
      console.log(mappedJsonData);
      this.totalSale = mappedJsonData.reduce((sum, item) => sum + parseInt(item.TotalPrice), 0);
      console.log(this.totalSale)
      this.dataSource = new MatTableDataSource<PeriodicElement>(mappedJsonData);
      this.dataSource.paginator = this.paginator;

      const toDate = str => new Date(str.replace(/^(\d+)\/(\d+)\/(\d+)$/, "$2/$1/$3"));
      const month = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
      const map = mappedJsonData.reduce((a, b) => {
        const m = toDate(b.Date).getMonth();
        a[m] = a[m] ? +a[m] + parseInt(b.TotalPrice) : + parseInt(b.TotalPrice);
        return a;
      }, {});
      this.monthSale = Object.entries(map).map(([key, TotalPrice]) => ({ TotalPrice, date: month[+key] }))

      
      const mostPopular = mappedJsonData.reduce((a, b) => {
        const m = toDate(b.Date).getMonth();
        a[m] = a[m] ? +a[m] + b.Quantity : +b.Quantity;
        return a;
      }, {});

      this.mostPopularinMonth = Object.entries(mostPopular).map(([key,Quantity])  => ({Quantity , date:month[+key]}))
      console.log(this.monthSale)
      console.log(this.mostPopularinMonth)


    });
  }


}
